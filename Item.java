/**
 * Class represent Item
 */
public class Item {
    // Fields of class
    private int cost;                           // Cost of item
    private int value;                          // Value of item


    /**
     * Constructor for initialization fields
     * @param value value of item
     * @param cost cost of item
     */
    Item(int value, int cost) {
        setValue(value);
        setCost(cost);
    }


    /**
     * Set cost of item
     * @param cost cost of item
     */
    public void setCost(int cost) { this.cost = cost; }


    /**
     * Set value of item
     * @param value
     */
    public void setValue(int value) { this.value = value; }


    /**
     * Getter for cost
     * @return return cost
     */
    public int getCost() { return cost; }

    /**
     * Getter for value
     * @return return value
     */
    public int getValue() { return value; }
}
